﻿using dg.interfaces;
using dg.interfaces.Providers;
using System.Collections.Generic;

namespace dg.utilities.Providers
{
    public class ValueProviderFabric : IValueProviderFabric
    {
        private ICache cache;
        private IContainer container;

        public ValueProviderFabric(ICache cache, IContainer container)
        {
            this.cache = cache;
            this.container = container;
        }

        public IValueProvider Get(string key, IDictionary<string, string> settings)
        {
            return container.Resolve<IValueProvider>(key, GetParameters(settings));
        }

        private IDictionary<string, object> GetParameters(IDictionary<string, string> settings)
        {
            return new Dictionary<string, object> { { "cache", cache }, { "settings", settings } };
        }
    }
}