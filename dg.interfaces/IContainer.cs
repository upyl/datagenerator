﻿using System.Collections.Generic;

namespace dg.interfaces
{
    public interface IContainer
    {
        T Resolve<T>(string name, IDictionary<string, object> constructorParameters = null);

        T Resolve<T>(IDictionary<string, object> constructorParameters = null);
    }
}