﻿namespace dg.interfaces
{
    public interface IWritableRepository<T>
    {
        void Write(T value);
    }
}