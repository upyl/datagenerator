﻿using dg.interfaces;
using dg.interfaces.Commands;
using dg.objects;
using dg.objects.Commands;
using dg.objects.Data;
using dg.objects.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dg.mssql.Commands
{
    public sealed class BulkInsertDataCommand : Command, IDisposable
    {
        private int columnNumber;
        private int commitBatchSize = 1000;
        private IList<object[]> currentValues = new List<object[]>();
        private string database;
        private ICommandPerformer<IBulkInsertDetails> executor;
        private IList<string> properties;
        private int recordCount = 0;
        private string schema;
        private EntityData value;

        public BulkInsertDataCommand(ICache cache, ICommandPerformer<IBulkInsertDetails> executor, EntityData value)
        {
            this.value = value;
            var database = cache.Pick<Database>();
            this.schema = database.Settings[Keys.NameSpace];
            this.database = database.Name;
            this.executor = executor;
            this.properties = database.Entities.Single(x => x.Name == value.EntityName).Properties.Select(x => x.Name).ToList();
            this.columnNumber = properties.Count();
        }

        public void Dispose()
        {
            if (recordCount > 0)
            {
                WriteToDatabase();
            }
        }

        protected override void ExecuteCore()
        {
            var groups = (from property in properties
                          join propertyData in value.PropertyData
                            on property equals propertyData.PropertyName
                          select propertyData).GroupBy(x => x.PropertyName).ToList();
            var data = new List<object[]>(groups.First().Count());
            for (var j = 0; j < groups.Count; j++)
            {
                var group = groups[j].ToList();
                for (var i = 0; i < group.Count; i++)
                {
                    if (data.Count <= i || data[i] == null)
                    {
                        data.Add(new object[groups.Count]);
                    }
                    data[i][j] = group[i].Value;
                }
            }

            foreach (var rec in data)
            {
                this.PopulateValues(rec);

                if (this.recordCount >= this.commitBatchSize)
                    this.WriteToDatabase();

                this.recordCount++;
            }
        }

        protected override void UnexecuteCore()
        {
            throw new NotImplementedException();
        }

        private void PopulateValues(object[] record)
        {
            if (this.properties.Count != record.Length)
            {
                throw new ArgumentOutOfRangeException("record");
            }

            currentValues.Add(record);
        }

        private void WriteToDatabase()
        {
            var columns = new Dictionary<int, string>();
            for(var i=0; i < columnNumber; i++)
            {
                columns.Add(i, properties[i]);
            }
            var bulkInfo = new BulkCommand(currentValues.GetEnumerator(), value.EntityName, columnNumber, schema, database, columns);
            executor.Execute(bulkInfo);
            this.recordCount = 0;
        }
    }
}