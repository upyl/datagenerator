﻿using dg.interfaces;
using dg.interfaces.Providers;
using dg.objects;
using System;
using System.Collections.Generic;

namespace dg.utilities.Providers
{
    public class RandomDayDateTimeProvider : IValueProvider
    {
        private DateTime end;
        private Random generator = new Random();
        private int range;

        private IDictionary<string, string> settings;

        private DateTime start;

        public RandomDayDateTimeProvider(ICache cache, IDictionary<string, string> settings)
        {
            this.settings = settings;
            start = Convert.ToDateTime(settings[Keys.DateTimeMinValue]);
            end = Convert.ToDateTime(settings[Keys.DateTimeMaxValue]);
            range = (end - start).Days;
        }

        public string GetNext()
        {
            return start.AddDays(generator.Next(range)).ToString(settings[Keys.DateTimeFormat]);
        }
    }
}