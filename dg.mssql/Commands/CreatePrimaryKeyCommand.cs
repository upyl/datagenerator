﻿using dg.interfaces.Commands;
using dg.objects;
using dg.objects.Commands;
using dg.objects.Database;

namespace dg.mssql.Commands
{
    public class CreatePrimaryKeyCommand : Command
    {
        private const string createRelation = "USE {0} ALTER TABLE {0}.{1}.{2} WITH CHECK ADD CONSTRAINT [PK_{2}] PRIMARY KEY({3})";
        private const string deleteRelation = "USE {0} ALTER TABLE {0}.{1}.{2} DROP CONSTRAINT [PK_{2}]";
        private string database;
        private ICommandPerformer<string> executor;
        private string field;
        private string schema;
        private string table;

        public CreatePrimaryKeyCommand(ICommandPerformer<string> exec, Property value)
        {
            this.database = value.Entity.Schema.Name;
            this.schema = value.Settings[Keys.NameSpace];
            this.table = value.Entity.Name;
            this.field = value.Name;
            this.executor = exec;
        }

        public override string ToString()
        {
            return string.Format("Database - {0}, Schema - {1}, Table - {2}, Field - {3}", database, schema,
                table, field);
        }

        protected override void ExecuteCore()
        {
            executor.Execute(string.Format(createRelation, database, schema, table, field));
        }

        protected override void UnexecuteCore()
        {
            executor.Execute(string.Format(deleteRelation, database, schema, table));
        }
    }
}