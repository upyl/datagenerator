﻿using dg.interfaces;
using dg.interfaces.Commands;
using System.Data.SqlClient;
using System.Threading;

namespace dg.mssql
{
    public class SqlCommandPerformer : ICommandPerformer<string>
    {
        private string connectionString;
        private ILog log;

        public SqlCommandPerformer(ILog log, string connectionString)
        {
            this.log = log;
            this.connectionString = connectionString;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public void Execute(string command)
        {
            using (log.Watch("SqlCommandPerformer.Execute", command))
            {
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = sqlConnection.CreateCommand();
                    sqlCommand.CommandText = command;
                    var result = sqlCommand.ExecuteNonQuery();
                }
            }
        }
    }
}