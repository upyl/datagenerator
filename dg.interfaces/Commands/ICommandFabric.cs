﻿using dg.objects;
using dg.objects.Commands;

namespace dg.interfaces.Commands
{
    public interface ICommandFabric
    {
        Command Get<K>(string key, K objectKey) where K : BaseObject;

        void ClearCache();
    }
}