﻿using dg.interfaces;
using System;
using System.Runtime.Caching;

namespace dg.utilities
{
    public class Cache : ICache
    {
        private MemoryCache cache = MemoryCache.Default;

        private ILog log;

        private CacheItemPolicy policy = new CacheItemPolicy();

        public Cache(ILog log, int expiration)
        {
            this.log = log;
            policy.SlidingExpiration = new TimeSpan(0, expiration, 0);
            policy.RemovedCallback = new CacheEntryRemovedCallback(this.MyCachedItemRemovedCallback);
        }

        public T Pick<T>()
        {
            return (T)cache.Get(GetTypeKey(typeof(T)));
        }

        public T Pick<T>(string key)
        {
            return (T)cache.Get(key);
        }

        public void Push<T>(T value)
        {
            cache.Set(GetTypeKey(typeof(T)), value, policy);
        }

        public void Push<T>(string key, T value)
        {
            cache.Set(key, value, policy);
        }

        private string GetTypeKey(Type type)
        {
            return type.GUID.ToString("N");
        }

        private void MyCachedItemRemovedCallback(CacheEntryRemovedArguments arguments)
        {
            String strLog = String.Concat("Cache Clean|Reason: ", arguments.RemovedReason.ToString(),
                "| Key-Name: ", arguments.CacheItem.Key, " | Value-Object: ", arguments.CacheItem.Value.ToString());
            //log.Trace(strLog);
        }
    }
}