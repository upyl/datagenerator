﻿using dg.interfaces;
using dg.interfaces.Commands;
using System.Data.SqlClient;

namespace dg.mssql
{
    public class BulkInsertCommandPerformer : ICommandPerformer<IBulkInsertDetails>
    {
        private string connectionString;
        private ILog log;

        public BulkInsertCommandPerformer(ILog log, string connectionString)
        {
            this.connectionString = connectionString;
            this.log = log;
        }

        public void Execute(IBulkInsertDetails details)
        {
            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(this.connectionString);
            using (log.Watch("BulkInsertCommandPerformer.Execute", string.Format("insert data to {0} database", sqlConnectionStringBuilder.InitialCatalog)))
            {
                using (SqlConnection connection =
                       new SqlConnection(sqlConnectionStringBuilder.ToString()))
                {
                    connection.Open();
                    connection.ExecuteBulk(details);
                }
            }
        }
    }
}