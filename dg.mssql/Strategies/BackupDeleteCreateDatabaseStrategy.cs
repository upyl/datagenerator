﻿using dg.interfaces;
using dg.interfaces.Commands;
using dg.objects;
using dg.objects.Commands;
using dg.objects.Database;
using System;
using System.Linq;
using System.Threading;

namespace dg.mssql.Strategies
{
    public class BackupDeleteCreateDatabaseStrategy : IStrategy<Database>
    {
        private ICache cache;
        private ICommandExecutor commandExecutor;
        private ICommandFabric commandFabric;
        private ILog log;
        Func<string, BaseObject, Command> construct;
        Action<Failure> fail;
        Action<Success> success;
        Action<Progress> progress;

        public BackupDeleteCreateDatabaseStrategy(ILog log, ICache cache, ICommandExecutor commandExecutor, ICommandFabric commandFabric)
        {
            this.log = log;
            this.cache = cache;
            this.commandFabric = commandFabric;
            this.commandExecutor = commandExecutor;
            this.construct = (name, db) => commandFabric.Get(name, db);
            this.fail = (f) =>
            {
                log.Error(f.Command.ToString(), f.Error);
                throw f.Error;
            };
            this.success = (s) => log.Trace(s.Command.ToString());
            this.progress = (p) => log.Trace(p.PercentDone.ToString());
        }

        public void Execute(Database database)
        {
            cache.Push(database.Key, database);

            ExecCommand(Keys.BackupDatabase, database);
            ExecCommand(Keys.DeleteDatabase, database);
            ExecCommand(Keys.CreateDatabase, database);
            foreach (var entity in database.Entities)
            {
                ExecCommand(Keys.CreateEntity, entity);
                if (entity.Properties.Any(x => x.Settings.Keys.Contains(Keys.IsPrimary)))
                {
                    ExecCommand(Keys.CreatePrimary, entity.Properties.Single(x => x.Settings.Keys.Contains(Keys.IsPrimary)));
                }
            }
            foreach (var relation in database.Entities.SelectMany(x => x.Relations))
            {
                ExecCommand(Keys.CreateRelation, relation);
            }
        }

        private void ExecCommand(string name, BaseObject bo)
        {
            commandExecutor.Execute<Command>(x => AugmentCommandDetails(x, name, bo));
        }

        private void AugmentCommandDetails(ICommandConfiguration<Command> x, string name, BaseObject bo)
        {
            x.ConstructUsing(() => construct(name, bo));
            x.OnFailure(f => fail(f));
            x.OnSuccess(s => success(s));
            x.OnProgress(p => progress(p));
        }
    }
}