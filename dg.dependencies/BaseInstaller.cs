﻿using dg.interfaces;
using dg.interfaces.Visitors;
using dg.objects.Database;
using dg.objects.DatabaseDescription;
using dg.utilities;
using dg.utilities.Validators;
using Microsoft.Practices.Unity;

namespace dg.dependencies
{
    public class BaseInstaller : IInstaller
    {
        public IUnityContainer Install(IUnityContainer container)
        {
            return container
                .RegisterType<ILog, Log>()
                .RegisterType<ICache, Cache>(new InjectionConstructor(new ResolvedParameter<ILog>(), 30))
                .Install<UtilsInstaller>()
                .Install<ServicesInstaller>()
                .Install<ValueProviderInstaller>()
                .RegisterType<IValidator<DatabaseDescription>, SchemaDescriptionValidator>()
                .RegisterType<IValidator<Database>, SchemaValidator>();
        }
    }
}