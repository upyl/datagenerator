﻿using dg.interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace dg.mssql
{
    public static class DbExtensions
    {
        public static void ExecuteBulk(this IDbConnection connection, IBulkInsertDetails bulkInsertDetails)
        {
            SqlConnection con = connection as SqlConnection;
            if (con == null) throw new NoNullAllowedException("connection");
            Action<KeyValuePair<int, string>> action = null;
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.TableLock, null))
            {
                if (action == null)
                {
                    action = mapping => bulkCopy.ColumnMappings.Add(mapping.Key, mapping.Value);
                }
                bulkInsertDetails.ColumnMappings.ToList<KeyValuePair<int, string>>().ForEach(action);
                bulkCopy.BulkCopyTimeout = bulkInsertDetails.BulkCopyTimeout;
                bulkCopy.DestinationTableName = bulkInsertDetails.TableName;
                bulkCopy.WriteToServer(bulkInsertDetails.GetDataReader());
            }
        }
    }
}