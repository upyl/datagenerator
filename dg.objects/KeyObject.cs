﻿using System;

namespace dg.objects
{
    public class KeyObject : IHasKey
    {
        private string key = Guid.NewGuid().ToString("N");

        public string Key
        {
            get { return key; }
        }
    }
}