﻿using System.Collections.Generic;

namespace dg.interfaces.Providers
{
    public interface IValueProviderFabric
    {
        IValueProvider Get(string name, IDictionary<string, string> settings);
    }
}