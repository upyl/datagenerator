﻿using dg.interfaces;
using dg.interfaces.Commands;
using dg.interfaces.Mappers;
using dg.interfaces.Providers;
using dg.mssql;
using dg.mssql.Commands;
using dg.mssql.Mappers;
using dg.mssql.Providers;
using dg.mssql.Strategies;
using dg.objects;
using dg.objects.Commands;
using dg.objects.Data;
using dg.objects.Database;
using Microsoft.Practices.Unity;

namespace dg.dependencies
{
    public class MsSqlInstaller : IInstaller
    {
        private string connectionString;

        public MsSqlInstaller(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public Microsoft.Practices.Unity.IUnityContainer Install(Microsoft.Practices.Unity.IUnityContainer container)
        {
            container
                .RegisterInstance<string>(Keys.ConnectionString, connectionString);

            container
                .Install<BaseInstaller>();

            container
                .RegisterType<ICommandPerformer<string>, SqlCommandPerformer>(new PerResolveLifetimeManager(), new InjectionConstructor(new ResolvedParameter<ILog>(), new ResolvedParameter<string>(Keys.ConnectionString)))
                .RegisterType<ICommandPerformer<IBulkInsertDetails>, BulkInsertCommandPerformer>(new PerResolveLifetimeManager(), new InjectionConstructor(new ResolvedParameter<ILog>(), new ResolvedParameter<string>(Keys.ConnectionString)));

            container
                .RegisterType<ICommandFabric, CommandFabric>()
                .RegisterType<Command, CreateDatabaseCommand>(Keys.CreateDatabase)
                .RegisterType<Command, CreatePrimaryKeyCommand>(Keys.CreatePrimary)
                .RegisterType<Command, BulkInsertDataCommand>(Keys.BulkInsertData, new PerResolveLifetimeManager())
                .RegisterType<Command, CreateForeignKeyCommand>(Keys.CreateRelation)
                .RegisterType<Command, CreateTableCommand>(Keys.CreateEntity)
                .RegisterType<Command, BackupDatabaseCommand>(Keys.BackupDatabase)
                .RegisterType<Command, InsertDataCommand>(Keys.InsertData);

            container
                .RegisterType<IStrategy<DatabaseData>, BulkInsertDataStrategy>()
                .RegisterType<IStrategy<Database>, BackupDeleteCreateDatabaseStrategy>();

            container
                .RegisterType<IValueProvider, DataProvider>(Keys.MsSqlDataProvider);

            return container
                    .RegisterType<ITypeMapper, TypeMapper>();
        }
    }
}