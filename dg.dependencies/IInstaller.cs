﻿using Microsoft.Practices.Unity;

namespace dg.dependencies
{
    public interface IInstaller
    {
        IUnityContainer Install(IUnityContainer container);
    }
}