﻿using dg.interfaces;
using dg.objects.Database;

namespace dg.services.Repository
{
    public class SchemaWriter : IWritableRepository<Database>
    {
        private IStrategy<Database> createDatabase;

        public SchemaWriter(IStrategy<Database> createDatabase)
        {
            this.createDatabase = createDatabase;
        }

        public void Write(Database value)
        {
            createDatabase.Execute(value);
        }
    }
}