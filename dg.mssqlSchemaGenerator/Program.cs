﻿using dg.mssql.schemagenerator;
using System;

namespace dg.mssqlSchemaGenerator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                throw new NotImplementedException("no connecting string");
            }
            var generator = new Generator(args[0]);
            generator.Execute();
        }
    }
}