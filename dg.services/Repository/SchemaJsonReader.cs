﻿using dg.objects.Database;

namespace dg.services.Repository
{
    public class SchemaJsonReader : JsonReader<Database>
    {
        public SchemaJsonReader(string fileName)
            : base(fileName)
        {
        }

        public override Database Read()
        {
            var result = base.Read();
            foreach (var entity in result.Entities)
            {
                UnionSchemaAndEntitySettings(result, entity);

                foreach (var property in entity.Properties)
                {
                    UnionEntityAndPropertySettings(entity, property);
                }

                foreach (var relation in entity.Relations)
                {
                    UnionEntityAndRelationSettings(entity, relation);
                }
            }
            return result;
        }

        private static void UnionEntityAndPropertySettings(Entity entity, Property property)
        {
            property.Entity = entity;
            foreach (var value in entity.Settings)
            {
                property.Settings.Add(value);
            }
        }

        private static void UnionSchemaAndEntitySettings(Database schema, Entity entity)
        {
            entity.Schema = schema;
            foreach (var value in schema.Settings)
            {
                entity.Settings.Add(value);
            }
        }

        private void UnionEntityAndRelationSettings(Entity entity, Relation relation)
        {
            relation.Entity = entity;
            foreach (var value in entity.Settings)
            {
                relation.Settings.Add(value);
            }
        }
    }
}