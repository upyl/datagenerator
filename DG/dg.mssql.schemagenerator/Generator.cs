﻿using dg.objects;
using dg.objects.Database;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace dg.mssql.schemagenerator
{
    public class Generator
    {
        private const string readColumns = "SELECT column_name 'Column Name', data_type 'Data Type', CHARacter_maximum_length 'Maximum Length', IS_NULLABLE FROM information_schema.columns WHERE TABLE_CATALOG = '{0}' and TABLE_SCHEMA = '{1}' and table_name = '{2}'";

        private const string readForeignKeys = @"SELECT
    K_Table = FK.TABLE_NAME,
    FK_Column = CU.COLUMN_NAME,
    PK_Table = PK.TABLE_NAME,
    PK_Column = PT.COLUMN_NAME,
    Constraint_Name = C.CONSTRAINT_NAME
FROM
    INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C
INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK
    ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME and FK.TABLE_NAME = '{2}'
INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK
    ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME
INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU
    ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME
INNER JOIN (
            SELECT
                i1.TABLE_NAME,
                i2.COLUMN_NAME
            FROM
                INFORMATION_SCHEMA.TABLE_CONSTRAINTS i1
            INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE i2
                ON i1.CONSTRAINT_NAME = i2.CONSTRAINT_NAME
            WHERE
                i1.CONSTRAINT_TYPE = 'PRIMARY KEY'
           ) PT
    ON PT.TABLE_NAME = PK.TABLE_NAME
WHERE C.CONSTRAINT_CATALOG='{0}' and C.CONSTRAINT_SCHEMA='{1}'";

        private const string readIdentityColumn = "use {0} SELECT NAME, SEED_VALUE, INCREMENT_VALUE FROM SYS.IDENTITY_COLUMNS where OBJECT_SCHEMA_NAME(OBJECT_ID) = '{1}' and OBJECT_NAME(OBJECT_ID) = '{2}'";

        private const string readPrimaryColumn = "SELECT kcu.COLUMN_NAME FROM [INFORMATION_SCHEMA].[TABLE_CONSTRAINTS] AS tc JOIN [INFORMATION_SCHEMA].[KEY_COLUMN_USAGE] AS kcu ON kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME WHERE tc.CONSTRAINT_TYPE = 'PRIMARY KEY' AND tc.TABLE_CATALOG = '{0}' and tc.TABLE_SCHEMA = '{1}' and tc.table_name = '{2}'";

        private const string readTables = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_CATALOG = '{0}' and TABLE_SCHEMA = '{1}'";

        private readonly SqlConnectionStringBuilder sqlConnectionBuilder;

        private bool generateUpdateSettings;

        public Generator(string connectionString, bool updateSettings = false)
        {
            this.sqlConnectionBuilder = new SqlConnectionStringBuilder(connectionString);
            this.generateUpdateSettings = updateSettings;
        }

        public void Execute()
        {
            var database = new Database();

            database.Settings[Keys.NameSpace] = "dbo";
            if (generateUpdateSettings)
            {
                database.Settings[Keys.Update] = "true";
            }
            database.Name = sqlConnectionBuilder.InitialCatalog;

            var tables = new List<Entity>();
            ExecuteReader(string.Format(readTables, database.Name, database.Settings[Keys.NameSpace]), (record) =>
            {
                tables.Add(new Entity { Name = (string)record[0] });
            });

            foreach (var table in tables)
            {
                var columns = new List<Property>();
                ExecuteReader(string.Format(readColumns, database.Name, database.Settings[Keys.NameSpace], table.Name), (record) =>
                {
                    var pr = new Property { Name = (string)record[0], Type = (string)record[1] + GetColumnLength(record) };
                    pr.Settings[Keys.IsNotSupportNull] = record[3].ToString() == "NO" ? "true" : "false";
                    columns.Add(pr);
                });
                ExecuteReader(string.Format(readPrimaryColumn, database.Name, database.Settings[Keys.NameSpace], table.Name), (record) =>
                {
                    columns.Single(x => x.Name == record[0].ToString()).Settings[Keys.IsPrimary] = "true";
                });

                ExecuteReader(string.Format(readIdentityColumn, database.Name, database.Settings[Keys.NameSpace], table.Name), (record) =>
                {
                    columns.Single(x => x.Name == record[0].ToString()).Settings[Keys.Identity] = string.Format("({0},{1})", record[1], record[2]);
                });

                var relations = new List<Relation>();
                ExecuteReader(string.Format(readForeignKeys, database.Name, database.Settings[Keys.NameSpace], table.Name), (record) =>
                {
                    relations.Add(new Relation { PrimaryEntity = table.Name, PrimaryProperty = record[1].ToString(), ForeignEntity = record[2].ToString(), ForeignProperty = record[3].ToString() });
                });
                table.Relations = relations;

                table.Properties = columns;
            }

            database.Entities = tables;

            WriteToFile(database);
        }

        private static string GetColumnLength(IDataReader record)
        {
            if ((string)record[1] == "nvarchar")
            {
                return (record[2].ToString() == string.Empty || record[2].ToString() == "-1" ? "(max)" : "(" + record[2] + ")");
            }
            return (record[2].ToString() != string.Empty ? "(" + record[2] + ")" : string.Empty);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public void ExecuteReader(string command, Action<IDataReader> action)
        {
            using (var sqlConnection = new SqlConnection(sqlConnectionBuilder.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = sqlConnection.CreateCommand();
                sqlCommand.CommandText = command;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    action(reader);
                }
            }
        }

        private void WriteToFile(Database database)
        {
            using (var f = File.CreateText("schema.json"))
            {
                f.Write(JsonConvert.SerializeObject(database, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, ContractResolver = new WritablePropertiesOnlyResolver() }));
            }
        }

        private class WritablePropertiesOnlyResolver : DefaultContractResolver
        {
            protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
            {
                IList<JsonProperty> props = base.CreateProperties(type, memberSerialization);
                return props.Where(p => p.Writable).ToList();
            }
        }
    }
}