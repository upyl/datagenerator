﻿using System.Collections.Generic;

namespace dg.objects.Database
{
    public class Database : BaseObject
    {
        private IEnumerable<Entity> entities = new List<Entity>();
        private string name;

        public IEnumerable<Entity> Entities
        {
            get { return entities; }
            set { this.entities = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}