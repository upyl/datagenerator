﻿namespace dg.interfaces.Visitors
{
    public interface IValidator<T>
    {
        void Validate(T value);
    }
}