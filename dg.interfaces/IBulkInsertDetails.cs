﻿using System.Collections.Generic;
using System.Data;

namespace dg.interfaces
{
    public interface IBulkInsertDetails
    {
        int BulkCopyTimeout { get; }

        IEnumerable<KeyValuePair<int, string>> ColumnMappings { get; }

        string TableName { get; }

        IDataReader GetDataReader();
    }
}