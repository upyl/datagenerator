﻿using dg.interfaces;
using dg.interfaces.Providers;
using dg.objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace dg.utilities.Providers
{
    public class RandomStringProvider : IValueProvider
    {
        private const string charactersL = "abcdefghijklmnopqrstuvwxyz";
        private const string charactersU = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string numbers = "1234567890";
        private static Random random = new Random();
        private StringBuilder builder = new StringBuilder();
        private string field;
        private int length;
        private IDictionary<string, string> settings;

        public RandomStringProvider(ICache cache, IDictionary<string, string> settings)
        {
            this.settings = settings;
            if (settings[Keys.Contains].Contains(Keys.UpperLetters))
            {
                field += charactersU;
            }
            if (settings[Keys.Contains].Contains(Keys.LowerLetters))
            {
                field += charactersL;
            }
            if (settings[Keys.Contains].Contains(Keys.Numbers))
            {
                field += numbers;
            }
            if (field.Length == 0)
            {
                field += charactersL;
            }
            length = Convert.ToInt32(settings[Keys.Length]);
        }

        public string GetNext()
        {
            builder.Clear();
            char ch;

            for (int i = 0; i < length; i++)
            {
                ch = field[random.Next(0, field.Length)];
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}