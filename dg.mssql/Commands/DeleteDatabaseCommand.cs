﻿using dg.interfaces.Commands;
using dg.objects.Database;

namespace dg.mssql.Commands
{
    public class DeleteDatabaseCommand : CreateDatabaseCommand
    {
        public DeleteDatabaseCommand(ICommandPerformer<string> performaer, Database value)
            : base(performaer, value)
        {
        }

        protected override void ExecuteCore()
        {
            base.UnexecuteCore();
        }

        protected override void UnexecuteCore()
        {
            base.ExecuteCore();
        }
    }
}