﻿using System.Collections.Generic;

namespace dg.objects.Database
{
    public class Entity : BaseObject
    {
        private string name;
        private IEnumerable<Property> properties = new List<Property>();
        private IEnumerable<Relation> relations = new List<Relation>();

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public IEnumerable<Property> Properties
        {
            get { return properties; }
            set { properties = value; }
        }

        public IEnumerable<Relation> Relations
        {
            get { return relations; }
            set { relations = value; }
        }

        public Database Schema { get; set; }
    }
}