﻿using dg.interfaces.Commands;
using dg.objects;
using dg.objects.Commands;
using dg.objects.Database;

namespace dg.mssql.Commands
{
    public class CreateForeignKeyCommand : Command
    {
        private const string createRelation = "USE {0} ALTER TABLE {0}.{1}.{2} WITH CHECK ADD CONSTRAINT [FK_{2}_{3}] FOREIGN KEY({4}) REFERENCES {0}.{1}.{3} ({5})";
        private const string deleteRelation = "USE {0} ALTER TABLE {0}.{1}.{2} DROP CONSTRAINT [FK_{2}_{3}]";
        private string database;
        private ICommandPerformer<string> executor;
        private string foreignProperty;
        private string foreignTable;
        private string primaryProperty;
        private string primaryTable;
        private string schema;

        public CreateForeignKeyCommand(ICommandPerformer<string> exec, Relation value)
        {
            this.database = value.Entity.Schema.Name;
            this.schema = value.Settings[Keys.NameSpace];
            this.primaryTable = value.PrimaryEntity;
            this.foreignTable = value.ForeignEntity;
            this.primaryProperty = value.PrimaryProperty;
            this.foreignProperty = value.ForeignProperty;
            this.executor = exec;
        }

        public override string ToString()
        {
            return string.Format("Database - {0}, Schema - {1}, PrimaryTable - {2}, PrimaryProperty - {3}, ForeignTable - {4}, ForeignProperty - {5}", database, schema,
                primaryTable, primaryProperty, foreignTable, foreignProperty);
        }

        protected override void ExecuteCore()
        {
            executor.Execute(string.Format(createRelation, database, schema, primaryTable, foreignTable, primaryProperty, foreignProperty));
        }

        protected override void UnexecuteCore()
        {
            executor.Execute(string.Format(deleteRelation, database, schema, primaryTable, foreignTable));
        }
    }
}