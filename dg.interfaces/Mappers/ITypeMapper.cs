﻿using System;

namespace dg.interfaces.Mappers
{
    public interface ITypeMapper
    {
        string FormatValue(string typeName, string value);

        Type GetType(string name);
    }
}