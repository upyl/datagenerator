﻿namespace dg.interfaces.Commands
{
    public interface ICommandPerformer<T>
    {
        void Execute(T reader);
    }

    public interface ICommandPerformer<T, K>
    {
        void Execute(T table, K reader);
    }
}