﻿using dg.interfaces;
using dg.interfaces.Commands;
using dg.interfaces.Mappers;
using dg.objects;
using dg.objects.Commands;
using dg.objects.Data;
using dg.objects.Database;
using System;
using System.Collections.Generic;

namespace dg.mssql.Commands
{
    public class CommandFabric : ICommandFabric
    {
        private const string keyFormat = "{0}_{1}";
        private ICommandPerformer<IBulkInsertDetails> bulkPerformer;
        private ICache cache;
        private IDictionary<string, Command> commands = new Dictionary<string, Command>();
        private ITypeMapper mapper;
        private ICommandPerformer<string> stringPerformer;

        public CommandFabric(ICommandPerformer<string> stringPerformer, ICommandPerformer<IBulkInsertDetails> bulkPerformer, ITypeMapper mapper, ICache cache)
        {
            this.stringPerformer = stringPerformer;
            this.mapper = mapper;
            this.bulkPerformer = bulkPerformer;
            this.cache = cache;
        }

        public Command Get<K>(string key, K input) where K : BaseObject
        {
            if (commands.ContainsKey(string.Format(keyFormat, key, input.Key)))
            {
                return commands[string.Format(keyFormat, key, input.Key)];
            }
            else
            {
                var command = GetCommand(key, input);
                commands.Add(string.Format(keyFormat, key, input.Key), command);
                return command;
            }
        }

        private Command GetCommand<K>(string key, K input) where K : BaseObject
        {
            switch (key)
            {
                case Keys.BackupDatabase:
                    return new BackupDatabaseCommand(stringPerformer, input as Database);

                case Keys.CreateDatabase:
                    return new CreateDatabaseCommand(stringPerformer, input as Database);

                case Keys.DeleteDatabase:
                    return new DeleteDatabaseCommand(stringPerformer, input as Database);

                case Keys.CreatePrimary:
                    return new CreatePrimaryKeyCommand(stringPerformer, input as Property);

                case Keys.BulkInsertData:
                    return new BulkInsertDataCommand(cache, bulkPerformer, input as EntityData);

                case Keys.CreateRelation:
                    return new CreateForeignKeyCommand(stringPerformer, input as Relation);

                case Keys.CreateEntity:
                    return new CreateTableCommand(stringPerformer, input as Entity);

                case Keys.InsertData:
                    return new InsertDataCommand(stringPerformer, cache, mapper, input as EntityData);

                default:
                    throw new KeyNotFoundException(key);
            }
        }


        public void ClearCache()
        {
            foreach(var command in commands.Values)
            {
                var disCommand = command as IDisposable;
                if (disCommand != null)
                {
                    disCommand.Dispose();
                }
            }
            commands = new Dictionary<string, Command>();
        }
    }
}