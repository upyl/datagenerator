﻿using dg.interfaces.Providers;
using System.Collections.Generic;

namespace dg.utilities.Providers
{
    public class EmptyStringProvider : IValueProvider
    {
        private IDictionary<string, string> settings;

        public EmptyStringProvider(IDictionary<string, string> settings)
        {
            this.settings = settings;
        }

        public string GetNext()
        {
            return string.Empty;
        }
    }
}