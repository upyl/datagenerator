﻿namespace dg.interfaces
{
    public interface IStrategy<T> where T : class
    {
        void Execute(T value);
    }
}