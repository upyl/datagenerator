﻿using dg.interfaces;
using dg.interfaces.Providers;
using dg.objects;
using System;
using System.Collections.Generic;

namespace dg.utilities.Providers
{
    public class RandomProxyProvider : IValueProvider
    {
        private ICache cache;
        private Random random;
        private IDictionary<string, string> settings;

        public RandomProxyProvider(ICache cache, IDictionary<string, string> settings)
        {
            this.cache = cache;
            this.settings = settings;
            this.random = new Random();
        }

        public string GetNext()
        {
            if (!settings.ContainsKey(Keys.Entity))
            {
                throw new KeyNotFoundException(Keys.Entity);
            }
            if (!settings.ContainsKey(Keys.Property))
            {
                throw new KeyNotFoundException(Keys.Property);
            }
            var key = Keys.CreateKey(settings[Keys.Entity], settings[Keys.Property]);
            var values = cache.Pick<IList<string>>(key);
            return values[random.Next(values.Count)];
        }
    }
}