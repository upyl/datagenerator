﻿namespace dg.objects.Commands
{
    public class Progress
    {
        public int PercentDone { get; set; }
    }
}