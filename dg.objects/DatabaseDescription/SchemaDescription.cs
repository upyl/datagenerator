﻿using System.Collections.Generic;

namespace dg.objects.DatabaseDescription
{
    public class DatabaseDescription : BaseObject
    {
        private IEnumerable<EntityDescription> entityDescriptions = new List<EntityDescription>();

        public string DatabaseName { get; set; }

        public IEnumerable<EntityDescription> EntityDescriptions
        {
            get { return entityDescriptions; }
            set { entityDescriptions = value; }
        }
    }
}