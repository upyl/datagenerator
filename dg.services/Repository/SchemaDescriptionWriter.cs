﻿using dg.interfaces;
using dg.interfaces.Providers;
using dg.objects;
using dg.objects.Data;
using dg.objects.DatabaseDescription;
using dg.utilities.Providers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dg.services.Repository
{
    public class SchemaDescriptionWriter : IWritableRepository<DatabaseDescription>
    {
        private ICache cache;
        private IStrategy<DatabaseData> insertDataStrategy;
        private IValueProviderFabric providerFabric;

        public SchemaDescriptionWriter(ICache cache, IStrategy<DatabaseData> insertDataStrategy, IValueProviderFabric providerFabric)
        {
            this.cache = cache;
            this.insertDataStrategy = insertDataStrategy;
            this.providerFabric = providerFabric;
        }

        public void Write(DatabaseDescription value)
        {
            var numberOfRowsInIteration = Convert.ToInt32(value.Settings[Keys.Iteration]);
            var iteration = 1;
            var providers = new Dictionary<string, Dictionary<string, IValueProvider>>();

            var needToWrap = NeedToCache(value);

            foreach (var entityDescription in value.EntityDescriptions)
            {
                var list = new Dictionary<string, IValueProvider>();
                foreach (var propertyDescription in entityDescription.PropertyDescriptions)
                {
                    var provider = providerFabric.Get(propertyDescription.ValueProviderName, propertyDescription.Settings);
                    if (needToWrap.Any(x => x.Item1 == entityDescription.EntityName && x.Item2 == propertyDescription.PropertyName))
                    {
                        propertyDescription.Settings.Add(Keys.Cache, Keys.CreateKey(entityDescription.EntityName, propertyDescription.PropertyName));
                        provider = new CacheProvider(cache, provider, propertyDescription.Settings);
                    }
                    list.Add(propertyDescription.PropertyName, provider);
                }
                providers.Add(entityDescription.EntityName, list);
            }
            using ((IDisposable)insertDataStrategy)
            {
                while (providers.Values.Any(x => x != null))
                {
                    var shemaData = new DatabaseData { DatabaseName = value.DatabaseName };
                    var entityList = new List<EntityData>();
                    shemaData.EntityData = entityList;
                    foreach (var entityDescription in OrderByDepedency(value.EntityDescriptions).Where(x => providers[x.EntityName] != null))
                    {
                        var numberOfRows = entityDescription.NumberOfRecords;
                        var entityData = new EntityData();
                        var list = new List<PropertyData>();
                        entityData.EntityName = entityDescription.EntityName;

                        foreach (var property in entityDescription.PropertyDescriptions)
                        {
                            for (var i = 0; i < (numberOfRows > numberOfRowsInIteration ? numberOfRowsInIteration : numberOfRows); i++)
                            {
                                list.Add(new PropertyData
                                {
                                    PropertyName = property.PropertyName,
                                    Value = providers[entityDescription.EntityName][property.PropertyName].GetNext()
                                });
                            }
                            entityDescription.NumberOfRecords = numberOfRows > numberOfRowsInIteration ? numberOfRows - numberOfRowsInIteration : 0;
                        }

                        if (entityDescription.NumberOfRecords == 0)
                        {
                            providers[entityDescription.EntityName] = null;
                        }
                        entityData.PropertyData = list;
                        entityList.Add(entityData);
                    }

                    iteration++;

                    if (iteration == 1000000) return;

                    insertDataStrategy.Execute(shemaData);
                }
            }
        }

        private static List<Tuple<string, string>> NeedToCache(DatabaseDescription value)
        {
            var needToWrap = new List<Tuple<string, string>>();

            foreach (var entityDescription in value.EntityDescriptions)
            {
                var list = new Dictionary<string, IValueProvider>();
                foreach (var propertyDescription in entityDescription.PropertyDescriptions)
                {
                    if (propertyDescription.Settings.ContainsKey(Keys.Entity) && propertyDescription.Settings.ContainsKey(Keys.Property))
                    {
                        needToWrap.Add(Tuple.Create(propertyDescription.Settings[Keys.Entity], propertyDescription.Settings[Keys.Property]));
                    }
                }
            }
            return needToWrap;
        }

        private IEnumerable<EntityDescription> OrderByDepedency(IEnumerable<EntityDescription> enumerable)
        {
            var lookup = enumerable.ToDictionary(x => x.EntityName, x => x);
            var iteration = 0;
            var dependencies = new List<Tuple<EntityDescription, List<EntityDescription>>>();
            foreach (var entityDescription in enumerable)
            {
                var tuple = Tuple.Create(entityDescription, new List<EntityDescription>());
                foreach (var propertyDescription in entityDescription.PropertyDescriptions)
                {
                    if (propertyDescription.Settings.ContainsKey(Keys.Entity))
                    {
                        tuple.Item2.Add(lookup[propertyDescription.Settings[Keys.Entity]]);
                    }
                }
                dependencies.Add(tuple);
            }
            var alreadyProceseed = new List<EntityDescription>();
            alreadyProceseed.AddRange(dependencies.Where(x => x.Item2.Count == 0).Select(x => x.Item1));
            while (alreadyProceseed.Count != enumerable.Count())
            {
                var addNext = dependencies.Where(x => !alreadyProceseed.Contains(x.Item1))
                    .Where(x => x.Item2.All(y => alreadyProceseed.Contains(y))).Select(x => x.Item1);
                alreadyProceseed.AddRange(addNext);
                if (iteration++ > 100) break;
            }
            return alreadyProceseed;
        }
    }
};