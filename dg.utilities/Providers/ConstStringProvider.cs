﻿using dg.interfaces.Providers;
using dg.objects;
using System.Collections.Generic;

namespace dg.utilities.Providers
{
    public class ConstStringProvider : IValueProvider
    {
        private IDictionary<string, string> settings;

        public ConstStringProvider(IDictionary<string, string> settings)
        {
            this.settings = settings;
        }

        public string GetNext()
        {
            return settings[Keys.Value];
        }
    }
}