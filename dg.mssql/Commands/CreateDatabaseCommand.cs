﻿using dg.interfaces.Commands;
using dg.objects.Commands;
using dg.objects.Database;

namespace dg.mssql.Commands
{
    public class CreateDatabaseCommand : Command
    {
        private const string createDatabase =
            @"IF (NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = '{0}' OR name = '{0}'))) CREATE DATABASE {0}";

        private const string deleteDatabase = @"IF (EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = '{0}' OR name = '{0}'))) DROP DATABASE {0}";
        private const string getRights = @"IF (EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = '{0}' OR name = '{0}'))) ALTER DATABASE {0} SET SINGLE_USER WITH ROLLBACK IMMEDIATE";

        private ICommandPerformer<string> commmandExecutor;
        private string database;

        public CreateDatabaseCommand(ICommandPerformer<string> performaer, Database value)
        {
            this.database = value.Name;
            this.commmandExecutor = performaer;
        }

        public override string ToString()
        {
            return string.Format("Database - {0}", database);
        }

        protected override void ExecuteCore()
        {
            commmandExecutor.Execute(string.Format(createDatabase, database));
        }

        protected override void UnexecuteCore()
        {
            commmandExecutor.Execute(string.Format(getRights, database));
            commmandExecutor.Execute(string.Format(deleteDatabase, database));
        }
    }
}