﻿namespace dg.mssql.Extensions
{
    public static class StringExtensions
    {
        public static bool ToBoolean(this string value)
        {
            bool result;
            if (bool.TryParse(value, out result))
            {
                return result;
            }
            return false;
        }
    }
}