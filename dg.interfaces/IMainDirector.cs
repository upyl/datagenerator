﻿using System.Collections.Generic;

namespace dg.interfaces
{
    public interface IMainDirector
    {
        void Construct(IEnumerable<IDataBuilder> builders);
    }
}