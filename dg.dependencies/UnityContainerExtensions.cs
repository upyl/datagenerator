﻿using Microsoft.Practices.Unity;
using System;

namespace dg.dependencies
{
    public static class UnityContainerExtensions
    {
        public static IUnityContainer Install(this IUnityContainer container, Type type)
        {
            var installer = Activator.CreateInstance(type) as IInstaller;
            if (installer != null)
            {
                return installer.Install(container);
            }
            return container;
        }

        public static IUnityContainer Install(this IUnityContainer container, IInstaller installer)
        {
            return installer.Install(container);
        }

        public static IUnityContainer Install<T>(this IUnityContainer container) where T : IInstaller, new()
        {
            return container.Install(typeof(T));
        }
    }
}