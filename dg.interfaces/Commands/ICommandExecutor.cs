﻿using dg.objects.Commands;
using System;

namespace dg.interfaces.Commands
{
    public interface ICommandExecutor
    {
        event EventHandler CanUndoRedoChanged;

        bool CanRedo { get; }

        bool CanUndo { get; }

        void Abort();

        void Execute<TCommand>(Action<ICommandConfiguration<TCommand>> action) where TCommand : Command;

        void Redo();

        void Undo();
    }
}