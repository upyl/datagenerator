﻿using dg.objects.Commands;
using System;

namespace dg.utilities.tests.TestObjects
{
    public class AlwaysThrows : Command
    {
        protected override void ExecuteCore()
        {
            throw new NotImplementedException();
        }

        protected override void UnexecuteCore()
        {
            throw new NotImplementedException();
        }
    }
}