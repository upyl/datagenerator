﻿namespace dg.objects
{
    public interface IHasKey
    {
        string Key { get; }
    }
}