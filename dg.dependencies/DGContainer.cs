﻿using dg.interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dg.dependencies
{
    public sealed class DGContainer : IContainer, IDisposable
    {
        private IUnityContainer container;

        public DGContainer()
        {
            this.container = new UnityContainer();
            container
                .RegisterInstance<IContainer>(this);
        }

        public void Dispose()
        {
            if (container != null) container.Dispose();
        }

        public void Init(IInstaller installer)
        {
            installer.Install(container);
        }

        public T Resolve<T>(string name, IDictionary<string, object> constructorParameters = null)
        {
            if (constructorParameters == null)
            {
                return container.Resolve<T>(name);
            }
            else
            {
                return container.Resolve<T>(name, constructorParameters.Select(x => new ParameterOverride(x.Key, x.Value)).ToArray());
            }
        }

        public T Resolve<T>(IDictionary<string, object> constructorParameters = null)
        {
            if (constructorParameters == null)
            {
                return container.Resolve<T>();
            }
            else
            {
                return container.Resolve<T>(constructorParameters.Select(x => new ParameterOverride(x.Key, x.Value)).ToArray());
            }
        }
    }
}