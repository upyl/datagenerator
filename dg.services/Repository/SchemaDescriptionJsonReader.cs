﻿using dg.objects.DatabaseDescription;

namespace dg.services.Repository
{
    public class SchemaDescriptionJsonReader : JsonReader<DatabaseDescription>
    {
        public SchemaDescriptionJsonReader(string fileName)
            : base(fileName)
        {
        }

        public override DatabaseDescription Read()
        {
            var result = base.Read();
            foreach (var entityDescription in result.EntityDescriptions)
            {
                UnionSchemaAndEntitySettings(result, entityDescription);

                foreach (var propertyDescription in entityDescription.PropertyDescriptions)
                {
                    UnionEntityAndPropertySettings(entityDescription, propertyDescription);
                }
            }
            return result;
        }

        private static void UnionEntityAndPropertySettings(EntityDescription entityDescription, PropertyDescription propertyDescription)
        {
            propertyDescription.EntityDescription = entityDescription;
            foreach (var entitySetting in entityDescription.Settings)
            {
                if (propertyDescription.Settings.ContainsKey(entitySetting.Key))
                {
                    propertyDescription.Settings[entitySetting.Key] = entitySetting.Value;
                }
                else
                {
                    propertyDescription.Settings.Add(entitySetting.Key, entitySetting.Value);
                }
            }
        }

        private static void UnionSchemaAndEntitySettings(DatabaseDescription value, EntityDescription entityDescription)
        {
            entityDescription.SchemaDescription = value;
            foreach (var schemaSettings in value.Settings)
            {
                if (entityDescription.Settings.ContainsKey(schemaSettings.Key))
                {
                    entityDescription.Settings[schemaSettings.Key] = schemaSettings.Value;
                }
                else
                {
                    entityDescription.Settings.Add(schemaSettings.Key, schemaSettings.Value);
                }
            }
        }
    }
}