﻿using System.Collections.Generic;

namespace dg.objects.DatabaseDescription
{
    public class EntityDescription : BaseObject
    {
        private int numberOfRecords;
        private IEnumerable<PropertyDescription> propertyDescriptions = new List<PropertyDescription>();

        public string EntityName { get; set; }

        public int NumberOfRecords
        {
            get { return numberOfRecords; }
            set { numberOfRecords = value; }
        }

        public IEnumerable<PropertyDescription> PropertyDescriptions
        {
            get { return propertyDescriptions; }
            set { propertyDescriptions = value; }
        }

        public DatabaseDescription SchemaDescription { get; set; }
    }
}