﻿using dg.interfaces.Commands;
using dg.objects;
using dg.objects.Commands;
using dg.objects.Database;
using System.Linq;

namespace dg.mssql.Commands
{
    public class BackupDatabaseCommand : Command
    {
        private const string backupDatabase =
            @"IF (EXISTS (SELECT name
FROM master.dbo.sysdatabases
WHERE ('[' + name + ']' = '{0}'
OR name = '{0}'))) BACKUP DATABASE {0} TO DISK = '{1}\{0}.bak' WITH FORMAT;";

        private const string restoreDatabase =
            @"RESTORE DATABASE {0} FROM DISK='{1}\{0}.bak'
WITH
   MOVE '{0}' TO '{2}\{0}.mdf',
   MOVE '{0}_Log' TO '{2}\{0}_log.ldf'";

        private string backupFolder;
        private ICommandPerformer<string> commmandExecutor;
        private string database;
        private string restoreFolder;

        public BackupDatabaseCommand(ICommandPerformer<string> exec, Database value)
        {
            this.backupFolder = value.Settings.Keys.Any(x => x == Keys.BackupFolder) ? value.Settings[Keys.BackupFolder] : null;
            this.restoreFolder = value.Settings.Keys.Any(x => x == Keys.RestoreFolder) ? value.Settings[Keys.RestoreFolder] : null;
            this.database = value.Name;
            this.commmandExecutor = exec;
        }

        public override string ToString()
        {
            return string.Format("Database - {0}, Backup folder - {1}", database, backupFolder);
        }

        protected override void ExecuteCore()
        {
            commmandExecutor.Execute(string.Format(backupDatabase, database, backupFolder));
        }

        protected override void UnexecuteCore()
        {
            commmandExecutor.Execute(string.Format(restoreDatabase, database, backupFolder, restoreFolder));
        }
    }
}