﻿using dg.interfaces;
using dg.services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace dg.tests
{
    [TestClass]
    public class DirectorTest
    {
        [TestMethod]
        public void DirectorPositive()
        {
            var monitor = string.Empty;
            var builder = new Mock<IDataBuilder>();
            builder.Setup(x => x.CreateStructure()).Callback(() => monitor += "1");
            builder.Setup(x => x.ValidateStructure()).Callback(() => monitor += "2");
            builder.Setup(x => x.WriteStructure()).Callback(() => monitor += "3");

            var director = new Director(new Mock<ILog>().Object);
            director.Construct(new[] { builder.Object });

            Assert.AreEqual(monitor, "123");
        }
    }
}