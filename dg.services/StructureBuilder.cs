﻿using dg.interfaces;
using dg.interfaces.Visitors;

namespace dg.services
{
    public class StructureBuilder<T> : IDataBuilder
    {
        private ILog log;
        private T structure;
        private IReadableRepository<T> structureReader;
        private IValidator<T> structureValidator;
        private IWritableRepository<T> structureWriter;

        public StructureBuilder(
            ILog log,
            IReadableRepository<T> structureReader,
            IWritableRepository<T> structureWriter,
            IValidator<T> structureValidator
            )
        {
            this.log = log;
            this.structureReader = structureReader;
            this.structureWriter = structureWriter;
            this.structureValidator = structureValidator;
        }

        public void CreateStructure()
        {
            using (log.Watch("CreateStructure"))
            {
                structure = structureReader.Read();
            }
        }

        public string GetStatus()
        {
            return string.Empty;
        }

        public void ValidateStructure()
        {
            using (log.Watch("ValidateStructure"))
            {
                structureValidator.Validate(structure);
            }
        }

        public void WriteStructure()
        {
            using (log.Watch("WriteStructure"))
            {
                structureWriter.Write(structure);
            }
        }
    }
}