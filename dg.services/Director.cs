﻿using dg.interfaces;
using System;
using System.Threading;

namespace dg.services
{
    public class Director : IMainDirector
    {
        private ILog log;

        public Director(ILog log)
        {
            this.log = log;
        }

        public void Construct(System.Collections.Generic.IEnumerable<IDataBuilder> builders)
        {
            try
            {
                foreach (var builder in builders)
                {
                    builder.CreateStructure();
                    builder.ValidateStructure();
                    builder.WriteStructure();
                    Thread.Sleep(500);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }
        }
    }
}