﻿using dg.objects.Commands;
using System;

namespace dg.interfaces.Commands
{
    public interface ICommandConfiguration<TCommand> where TCommand : Command
    {
        void ConstructUsing(Func<TCommand> commandFactory);

        void OnFailure(Action<Failure> handleFailure);

        void OnProgress(Action<Progress> handleProgress);

        void OnSuccess(Action<Success> handleSuccess);
    }
}