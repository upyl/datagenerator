﻿using dg.interfaces.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dg.mssql.Mappers
{
    public class TypeMapper : ITypeMapper
    {
        private static IDictionary<string, Type> internalStructure = new Dictionary<string, Type> {
            {"bigint", typeof(Int64)},
            {"binary", typeof(Byte[])},
            {"bit", typeof(String)},
            {"date", typeof(DateTime)},
            {"datetime", typeof(DateTime)},
            {"datetime2", typeof(DateTime)},
            {"datetimeoffset", typeof(DateTimeOffset)},
            {"decimal", typeof(Decimal)},
            {"varbinary", typeof(Byte[])},
            {"float", typeof(Double)},
            {"image", typeof(Byte[])},
            {"int", typeof(Int32)},
            {"nchar", typeof(String)},
            {"ntext", typeof(String)},
            {"numeric", typeof(Decimal)},
            {"nvarchar", typeof(String)},
            {"real", typeof(Single)},
            {"rowversion", typeof(Byte[])},
            {"smalldatetime", typeof(DateTime)},
            {"smallint", typeof(Int16)},
            {"smallmoney", typeof(Decimal)},
            {"sql_variant", typeof(Object)},
            {"text", typeof(String)},
            {"time", typeof(TimeSpan)},
            {"timestamp", typeof(Byte[])},
            {"tinyint", typeof(Byte)},
            {"uniqueidentifier", typeof(Guid)},
            {"varchar", typeof(String)},
            {"xml", typeof(System.Xml.XmlNode)}
        };

        public string FormatValue(string typeName, string value)
        {
            if (GetType(typeName) == typeof(String) || GetType(typeName) == typeof(DateTime))
                return string.Format("'{0}'", value);
            return value;
        }

        public Type GetType(string name)
        {
            var key = internalStructure.Keys.FirstOrDefault(x => name.Contains(x));
            if (key != null)
                return internalStructure[key];
            return typeof(Object);
        }
    }
}