﻿namespace dg.interfaces
{
    public interface IReadableRepository<T>
    {
        T Read();
    }
}