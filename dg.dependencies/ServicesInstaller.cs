﻿using dg.interfaces;
using dg.objects.Database;
using dg.objects.DatabaseDescription;
using dg.services;
using dg.services.Repository;
using Microsoft.Practices.Unity;

namespace dg.dependencies
{
    public class ServicesInstaller : IInstaller
    {
        public Microsoft.Practices.Unity.IUnityContainer Install(Microsoft.Practices.Unity.IUnityContainer container)
        {
            return container
                .RegisterType<IMainDirector, Director>()
                .RegisterType<IDataBuilder, StructureBuilder<Database>>("schema")
                .RegisterType<IDataBuilder, StructureBuilder<DatabaseDescription>>("schemaDescription")
                .RegisterType<IReadableRepository<Database>, SchemaJsonReader>(new InjectionConstructor("schema.json"))
                .RegisterType<IReadableRepository<DatabaseDescription>, SchemaDescriptionJsonReader>(new InjectionConstructor("schemaDescription.json"))
                .RegisterType<IWritableRepository<Database>, SchemaWriter>()
                .RegisterType<IWritableRepository<DatabaseDescription>, SchemaDescriptionWriter>();
        }
    }
}