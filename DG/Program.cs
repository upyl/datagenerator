﻿using dg.dependencies;
using dg.interfaces;
using System;

namespace DG
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var container = new DGContainer();

            var installer = new MsSqlInstaller("Server=FES-PC\\SQLEXPRESS;Trusted_Connection=True;");

            container.Init(installer);

            IMainDirector director = container.Resolve<IMainDirector>();
            var builders = new[]{
                container.Resolve<IDataBuilder>("schema"),
                container.Resolve<IDataBuilder>("schemaDescription")};
            director.Construct(builders);
            var result = string.Empty;
            foreach (var builder in builders)
            {
                result += builder.GetStatus();
            }
            Console.WriteLine(result);
        }
    }
}