﻿using dg.interfaces;
using dg.interfaces.Providers;
using dg.objects;
using System.Collections.Generic;

namespace dg.utilities.Providers
{
    public class CacheProvider : IValueProvider
    {
        private ICache cache;
        private IValueProvider provider;
        private IDictionary<string, string> settings;

        public CacheProvider(ICache cache, IValueProvider provider, IDictionary<string, string> settings)
        {
            this.cache = cache;
            this.provider = provider;
            this.settings = settings;
        }

        public string GetNext()
        {
            if (!settings.ContainsKey(Keys.Cache))
            {
                throw new KeyNotFoundException(Keys.Cache);
            }
            var key = settings[Keys.Cache];
            var value = provider.GetNext();

            IList<string> list = cache.Pick<IList<string>>(key);
            if (list == null)
            {
                list = new List<string>();
            }
            list.Add(value);
            cache.Push<IList<string>>(key, list);
            return value;
        }
    }
}