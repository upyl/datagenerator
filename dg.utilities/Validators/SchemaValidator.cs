﻿using dg.interfaces;
using dg.interfaces.Mappers;
using dg.interfaces.Visitors;
using dg.objects;
using dg.objects.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dg.utilities.Validators
{
    public class SchemaValidator : IValidator<Database>
    {
        private ICache cache;
        private ITypeMapper mapper;

        public SchemaValidator(ICache cache, ITypeMapper mapper)
        {
            this.cache = cache;
            this.mapper = mapper;
        }

        public void Validate(Database value)
        {
            CheckPropertyTypes(value);
            CheckRelations(value);
            CheckSettings(value);
            cache.Push<Database>(value);
        }

        private static void CheckRelations(Database value)
        {
            foreach (var entity in value.Entities)
            {
                foreach (var relation in entity.Relations)
                {
                    if (relation.PrimaryEntity != entity.Name)
                        throw new NotSupportedException(string.Format("relation.PrimaryEntity - {0}", relation.PrimaryEntity));
                    if (entity.Properties.All(x => x.Name != relation.PrimaryProperty))
                        throw new NotSupportedException(string.Format("relation.PrimaryProperty - {0}", relation.PrimaryProperty));
                    if (value.Entities.All(x => x.Name != relation.ForeignEntity))
                        throw new NotSupportedException(string.Format("relation.ForeignEntity - {0}", relation.ForeignEntity));
                    if (value.Entities.SelectMany(x => x.Properties).All(x => x.Name != relation.ForeignProperty))
                        throw new NotSupportedException(string.Format("relation.ForeignProperty - {0}", relation.ForeignProperty));
                }
            }
        }

        private static void CheckSettings(Database value)
        {
            var keys = Keys.GetAllKeys();
            CheckThatSettingIsSupported(value.Settings, keys);
            foreach (var entity in value.Entities)
            {
                CheckThatSettingIsSupported(entity.Settings, keys);
                foreach (var property in entity.Properties)
                {
                    CheckThatSettingIsSupported(property.Settings, keys);
                }
                foreach (var relation in entity.Relations)
                {
                    CheckThatSettingIsSupported(relation.Settings, keys);
                }
            }
        }

        private static void CheckThatSettingIsSupported(IDictionary<string, string> settings, System.Collections.Generic.IDictionary<string, string> keys)
        {
            foreach (var setting in settings)
            {
                if (keys.Values.All(x => x != setting.Key))
                    throw new NotSupportedException(string.Format("setting {0}", setting.Key));
                Keys.CheckKeySupportValue(setting.Key, setting.Value);
            }
        }

        private void CheckPropertyTypes(Database value)
        {
            foreach (var entity in value.Entities)
            {
                foreach (var property in entity.Properties)
                {
                    var type = mapper.GetType(property.Type);
                    if (type == typeof(object))
                        throw new NotSupportedException(property.Type);
                }
            }
        }
    }
}