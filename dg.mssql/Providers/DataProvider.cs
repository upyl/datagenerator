﻿using dg.interfaces;
using dg.interfaces.Providers;
using dg.objects;
using System;
using System.Collections.Generic;

namespace dg.mssql.Providers
{
    public class DataProvider : IValueProvider
    {
        private const string columnName = @"SELECT DATA_TYPE
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_CATALOG = {0}
     TABLE_NAME = '{2}' AND TABLE_SCHEMA = '{1}'
     COLUMN_NAME = '{3}'";

        private ICache cache;
        private IDictionary<string, string> settings;
        private string sourceColumn;
        private string sourceConnectionString;
        private string sourceSchema;
        private string sourceTable;

        public DataProvider(ICache cache, IDictionary<string, string> settings)
        {
            this.cache = cache;
            this.sourceConnectionString = settings[Keys.SourceConnectionString];
            this.sourceTable = settings[Keys.SourceTable];
            this.sourceColumn = settings[Keys.SourceColumn];
            this.sourceSchema = settings[Keys.SourceSchema];
            this.settings = settings;
        }

        public string GetNext()
        {
            throw new NotImplementedException();
        }

        private void LoadData()
        {
        }
    }
}