﻿using System;

namespace dg.objects.Commands
{
    public class Failure
    {
        private readonly Command command;

        private readonly Exception error;

        public Failure(Command command, Exception error)
        {
            this.command = command;
            this.error = error;
        }

        public Command Command
        {
            get
            {
                return this.command;
            }
        }

        public Exception Error
        {
            get
            {
                return this.error;
            }
        }
    }
}