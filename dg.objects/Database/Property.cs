﻿namespace dg.objects.Database
{
    public class Property : BaseObject
    {
        private string name;
        private string type;

        public Entity Entity { get; set; }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }
    }
}