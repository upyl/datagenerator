﻿using dg.interfaces;
using dg.interfaces.Commands;
using dg.interfaces.Mappers;
using dg.objects;
using dg.objects.Commands;
using dg.objects.Data;
using dg.objects.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dg.mssql.Commands
{
    public class InsertDataCommand : Command
    {
        private const string insertDataWithIdentity =
            @"SET IDENTITY_INSERT {0}.{1}.{2} ON;
INSERT INTO {0}.{1}.{2} ({3}) VALUES ({4});
SET IDENTITY_INSERT {0}.{1}.{2} OFF;";

        private const string insertDataWithoutIdentity =
            @"INSERT INTO {0}.{1}.{2} ({3}) VALUES ({4});";

        private ICache cache;
        private ICommandPerformer<string> executor;
        private ITypeMapper mapper;
        private string nameSpace;
        private IEnumerable<Property> properties;
        private Database schema;
        private EntityData value;

        public InsertDataCommand(ICommandPerformer<string> exec, ICache cache, ITypeMapper mapper, EntityData value)
        {
            this.value = value;
            this.cache = cache;
            this.executor = exec;
            this.schema = cache.Pick<Database>();
            this.nameSpace = schema.Settings[Keys.NameSpace];
            this.mapper = mapper;
            this.properties = schema.Entities.Single(x => x.Name == value.EntityName).Properties;
        }

        protected override void ExecuteCore()
        {
            List<string> propertyValues = new List<string>();
            var propertyDescription = string.Empty;
            var propertyDescriptions = value.PropertyData.ToLookup(x => x.PropertyName, y => y.Value);
            var prevCount = propertyDescriptions.First().Count();

            foreach (var gr in propertyDescriptions.Skip(1))
            {
                if (gr.Count() != prevCount) throw new ArgumentException("EntityData");
            }

            foreach (var property in propertyDescriptions)
            {
                propertyDescription += property.Key + ",";
            }

            if (propertyDescription.EndsWith(","))
            {
                propertyDescription = propertyDescription.Substring(0, propertyDescription.Length - 1);
            }

            for (var i = 0; i < prevCount; i++)
            {
                var propertyValueDescription = string.Empty;
                foreach (var propertyValue in propertyDescriptions)
                {
                    propertyValueDescription += mapper.FormatValue(properties.Single(x => x.Name == propertyValue.Key).Type, propertyValue.Skip(i).First()) + ",";
                }
                if (propertyValueDescription.EndsWith(","))
                {
                    propertyValueDescription = propertyValueDescription.Substring(0, propertyValueDescription.Length - 1);
                }
                propertyValues.Add(propertyValueDescription);
            }

            foreach (var v in propertyValues)
            {
                if (properties.Any(x => x.Settings.Any(z => z.Key == Keys.Identity)))
                {
                    executor.Execute(string.Format(insertDataWithIdentity, schema.Name, nameSpace, value.EntityName, propertyDescription, v));
                }
                else
                {
                    executor.Execute(string.Format(insertDataWithoutIdentity, schema.Name, nameSpace, value.EntityName, propertyDescription, v));
                }
            }
        }

        protected override void UnexecuteCore()
        {
            throw new NotImplementedException();
        }
    }
}