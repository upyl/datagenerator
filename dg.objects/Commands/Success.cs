﻿namespace dg.objects.Commands
{
    public class Success
    {
        private readonly Command command;

        public Success(Command command)
        {
            this.command = command;
        }

        public Command Command
        {
            get
            {
                return this.command;
            }
        }
    }
}