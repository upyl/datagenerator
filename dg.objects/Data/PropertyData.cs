﻿namespace dg.objects.Data
{
    public class PropertyData : BaseObject
    {
        public string PropertyName { get; set; }

        public string Value { get; set; }
    }
}