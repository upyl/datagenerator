﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace dg.objects
{
    public static class Keys
    {
        public const string BackupDatabase = "BackupDatabase";
        public const string BackupFolder = "BackupFolder";
        public const string BulkInsertData = "bulkInsertData";
        public const string Cache = "Cache";
        public const string ConnectionString = "connectionString";
        public const string ConstStringProvider = "ConstStringProvider";
        public const string Contains = "Contains";
        public const string CreateDatabase = "CreateDatabase";
        public const string CreateEntity = "createEntity";
        public const string CreatePrimary = "CreatePrimary";
        public const string CreateRelation = "createRelation";
        public const string CustomStringProvider = "CustomStringProvider";
        public const string DateTimeFormat = "DateTimeFormat";
        public const string DateTimeMaxValue = "DateTimeMaxValue";
        public const string DateTimeMinValue = "DateTimeMinValue";
        public const string DeleteDatabase = "DeleteDatabase";
        public const string Entity = "Entity";
        public const string Identity = "Identity";
        public const string IdentityDayDateTimeProvider = "IdentityDayDateTimeProvider";
        public const string Increment = "Increment";
        public const string InsertData = "insertData";
        public const string IsNotSupportNull = "IsNotSupportNull";
        public const string IsPrimary = "IsPrimary";
        public const string Iteration = "Iteration";
        public const string Length = "Length";
        public const string LowerLetters = "LowerLetters";
        public const string MsSqlDataProvider = "MsSqlDataProvider";
        public const string NameSpace = "NameSpace";
        public const string NumberIdentityProvider = "NumberIdentityProvider";
        public const string Numbers = "Numbers";
        public const string Property = "Property";
        public const string RandomDayDateTimeProvider = "RandomDayDateTimeProvider";
        public const string RandomProxyProvider = "RandomProxyProvider";
        public const string RandomStringProvider = "RandomStringProvider";
        public const string RestoreFolder = "RestoreFolder";
        public const string SourceColumn = "SourceColumn";
        public const string SourceConnectionString = "SourceConnectionString";
        public const string SourceSchema = "SourceSchema";
        public const string SourceTable = "SourceTable";
        public const string Start = "Start";
        public const string Update = "Update";
        public const string UpdateDatabase = "UpdateDatabase";
        public const string UpperLetters = "UpperLetters";
        public const string Value = "Value";
        public const string Values = "Values";

        private static IDictionary<string, string> KeyPossibleValues = new Dictionary<string, string>
        {
            {Keys.IsPrimary, "^(true|false)$"},
            {Keys.Update, "^(true|false)$"},
            {Keys.IsNotSupportNull, "^(true|false)$"},
            {Keys.Contains, @"^(UpperLetters|LowerLetters|Numbers)?;?(UpperLetters|LowerLetters|Numbers)?;?(UpperLetters|LowerLetters|Numbers)?"},
            {Keys.Identity, @"^\(\d+,\d+\)$"},
            {Keys.Length, @"^\d+$"},
            {Keys.Iteration, @"^\d+$"},
            {Keys.Start, @"^\d+$"},
            {Keys.NameSpace, @"^\w+$"},
            {Keys.Property, @"^\w+$"},
            {Keys.BackupFolder, @"^(?:[\w]\:|\\)(\\[a-z_\-\s0-9\.]+)*\\$"},
            {Keys.RestoreFolder, @"^(?:[\w]\:|\\)(\\[a-z_\-\s0-9\.]+)*\\$"}
        };

        public static void CheckKeySupportValue(string key, string value)
        {
            if (KeyPossibleValues.Keys.All(x => x != key)) return;
            if (!Regex.IsMatch(value, KeyPossibleValues[key]))
                throw new NotSupportedException(string.Format("key {0}: value {1}", key, value));
        }

        public static string CreateKey(params string[] values)
        {
            return string.Join(".", values);
        }

        public static IDictionary<string, string> GetAllKeys()
        {
            return typeof(Keys)
              .GetFields(BindingFlags.Public | BindingFlags.Static)
              .Where(f => f.FieldType == typeof(string) && f.IsLiteral && f.IsStatic)
              .ToDictionary(f => f.Name, f => (string)f.GetValue(null));
        }
    }
}