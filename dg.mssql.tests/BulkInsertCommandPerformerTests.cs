﻿using dg.interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace dg.mssql.tests
{
    [TestClass]
    public class BulkInsertCommandPerformerTests
    {
        private const string connectionString = "Server=FES-PC\\SQLEXPRESS;Trusted_Connection=True;";

        [TestInitialize]
        public void ClearForTest()
        {
            // TODO: Create test database , create test table
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                var com = con.CreateCommand();
                com.CommandText = "DELETE FROM testDg.dbo.roles";
                com.ExecuteNonQuery();
            }
        }

        [TestMethod]
        public void InsertDataByBulkInsertTest()
        {
            var log = new Mock<ILog>().Object;
            
            var bulkPerformer = new BulkInsertCommandPerformer(log, connectionString);

            var toInser = new List<object[]>{
                new object[]{1, "test"},
                new object[]{2, "test3"},
                new object[]{3, "test2"}
            };

            var details = new BulkCommand(toInser.GetEnumerator(), "roles", 2, "dbo", "testDg", new Dictionary<int, string> { {0, "id"}, {1, "name"}});

            bulkPerformer.Execute(details);
        }
    }
}