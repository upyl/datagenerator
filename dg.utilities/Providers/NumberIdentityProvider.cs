﻿using dg.interfaces;
using dg.interfaces.Providers;
using dg.objects;
using System;
using System.Collections.Generic;

namespace dg.utilities.Providers
{
    public class NumberIdentityProvider : IValueProvider
    {
        private int increment;
        private int index;
        private IDictionary<string, string> settings;

        public NumberIdentityProvider(ICache cache, IDictionary<string, string> settings)
        {
            this.settings = settings;
            index = settings.ContainsKey(Keys.Start) ? Convert.ToInt32(settings[Keys.Start]) : 1;
            increment = settings.ContainsKey(Keys.Increment) ? Convert.ToInt32(settings[Keys.Increment]) : 1;
        }

        public string GetNext()
        {
            var t = index;
            index += increment;
            return t.ToString();
        }
    }
}