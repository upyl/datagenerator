﻿using dg.interfaces.Providers;
using dg.objects;
using dg.utilities.Providers;
using Microsoft.Practices.Unity;

namespace dg.dependencies
{
    public class ValueProviderInstaller : IInstaller
    {
        public Microsoft.Practices.Unity.IUnityContainer Install(Microsoft.Practices.Unity.IUnityContainer container)
        {
            return container
               .RegisterType<IValueProvider, CustomStringProvider>(Keys.CustomStringProvider)
               .RegisterType<IValueProvider, RandomStringProvider>(Keys.RandomStringProvider)
               .RegisterType<IValueProvider, IdentityDayDateTimeProvider>(Keys.IdentityDayDateTimeProvider)
               .RegisterType<IValueProvider, RandomDayDateTimeProvider>(Keys.RandomDayDateTimeProvider)
               .RegisterType<IValueProvider, NumberIdentityProvider>(Keys.NumberIdentityProvider)
               .RegisterType<IValueProvider, RandomProxyProvider>(Keys.RandomProxyProvider)
               .RegisterType<IValueProvider, ConstStringProvider>(Keys.ConstStringProvider)
               .RegisterType<IValueProvider, EmptyStringProvider>();
        }
    }
}