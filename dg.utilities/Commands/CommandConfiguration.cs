﻿using dg.interfaces.Commands;
using dg.objects.Commands;
using System;

namespace dg.utilities.Commands
{
    public abstract class CommandConfiguration
    {
        public abstract void Execute();

        public abstract void Unexecute();
    }

    public class CommandConfiguration<TCommand> : CommandConfiguration, ICommandConfiguration<TCommand>
        where TCommand : Command
    {
        private TCommand command;
        private Func<TCommand> commandFactory;
        private Action<Action> executor;
        private Action<Failure> handleFailure;
        private Action<Progress> handleProgress;
        private Action<Success> handleSuccess;

        public CommandConfiguration()
        {
            this.handleSuccess = cmd => { };
            this.handleFailure = failureInfo => { };
            this.handleProgress = progressInfo => { };

            this.executor = action => action();
            this.commandFactory = () => Activator.CreateInstance<TCommand>();
        }

        public TCommand Command
        {
            get
            {
                if (this.command == null)
                {
                    this.command = this.commandFactory();
                }

                return this.command;
            }
        }

        public void ConstructUsing(Func<TCommand> commandFactory)
        {
            this.commandFactory = commandFactory;
        }

        public override void Execute()
        {
            try
            {
                this.Command.Progress = progress => this.executor(() => this.handleProgress(progress));

                this.Command.Execute();

                this.executor(() => this.handleSuccess(new Success(this.Command)));
            }
            catch (Exception ex)
            {
                this.executor(() => this.handleFailure(new Failure(this.Command, ex)));
            }
        }

        public void OnFailure(Action<Failure> handleFailure)
        {
            this.handleFailure = handleFailure;
        }

        public void OnProgress(Action<Progress> handleProgress)
        {
            this.handleProgress = handleProgress;
        }

        public void OnSuccess(Action<Success> handleSuccess)
        {
            this.handleSuccess = handleSuccess;
        }

        public override void Unexecute()
        {
            try
            {
                this.Command.Unexecute();

                this.executor(() => this.handleSuccess(new Success(this.Command)));
            }
            catch (Exception ex)
            {
                this.executor(() => this.handleFailure(new Failure(this.Command, ex)));
            }
        }
    }
}