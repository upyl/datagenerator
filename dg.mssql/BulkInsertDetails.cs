﻿using dg.interfaces;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace dg.mssql
{
    public class BulkCommand : BulkInsertDetails
    {
        private IEnumerator<object[]> data;
        private IDictionary<int, string> columns;

        public BulkCommand(IEnumerator<object[]> data, string tableName, int fieldCount, string schema, string database, IDictionary<int, string> columns)
            : base(tableName, fieldCount, schema, database)
        {
            this.columns = columns;
            this.data = data;
        }

        public override IEnumerable<KeyValuePair<int, string>> ColumnMappings
        {
            get
            {
                return this.columns.AsEnumerable();
            }
        }

        public override IEnumerator<object[]> GetDataEnumerator()
        {
            return data;
        }

        public override IDataReader GetDataReader()
        {
            return new ObjectDataReader(this);
        }
    }

    public abstract class BulkInsertDetails : IBulkInsertDetails, IDataReadable
    {
        private int fieldCount;
        private string tableName;

        protected BulkInsertDetails(string tableName, int fieldCount, string schema, string database)
        {
            this.tableName = string.Format("{0}.{1}.{2}", database, schema, tableName);
            this.fieldCount = fieldCount;
        }

        public int BulkCopyTimeout
        {
            get { return 600; }
        }

        public virtual IEnumerable<KeyValuePair<int, string>> ColumnMappings
        {
            get
            {
                return new Dictionary<int, string>();
            }
        }

        public virtual int FieldCount
        {
            get { return this.fieldCount; }
        }

        public virtual string TableName
        {
            get { return this.tableName; }
        }

        public abstract IEnumerator<object[]> GetDataEnumerator();

        public abstract IDataReader GetDataReader();
    }
}