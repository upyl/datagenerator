﻿using dg.interfaces;
using dg.interfaces.Commands;
using dg.objects;
using dg.objects.Commands;
using dg.objects.Data;
using System;
using System.Collections.Generic;

namespace dg.mssql.Strategies
{
    public sealed class BulkInsertDataStrategy : IStrategy<DatabaseData>, IDisposable
    {
        private ICommandFabric commandFabric;
        private ILog log;
        private ICommandExecutor commandExecutor;
        Func<string, BaseObject, Command> construct;
        Action<Failure> fail;
        Action<Success> success;
        Action<Progress> progress;

        public BulkInsertDataStrategy(ILog log, ICache cache, ICommandExecutor commandExecutor, ICommandFabric commandFabric)
        {
            this.log = log;
            this.commandExecutor = commandExecutor;
            this.commandFabric = commandFabric;
            this.construct = (name, db) => commandFabric.Get(name, db);
            this.fail = (f) => log.Error(f.Command.ToString(), f.Error);
            this.success = (s) => log.Trace(s.Command.ToString());
            this.progress = (p) => log.Trace(p.PercentDone.ToString());
        }

        private void ExecCommand(string name, BaseObject bo)
        {
            commandExecutor.Execute<Command>(x => AugmentCommandDetails(x, name, bo));
        }

        private void AugmentCommandDetails(ICommandConfiguration<Command> x, string name, BaseObject bo)
        {
            x.ConstructUsing(() => construct(name, bo));
            x.OnFailure(f => fail(f));
            x.OnSuccess(s => success(s));
            x.OnProgress(p => progress(p));
        }

        public void Execute(DatabaseData databaseData)
        {
            foreach (var entity in databaseData.EntityData)
            {
                ExecCommand(Keys.BulkInsertData, entity);
            }
        }

        public void Dispose()
        {
            if (commandFabric != null)
                commandFabric.ClearCache();
        }
    }
}