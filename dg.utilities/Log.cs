﻿using dg.interfaces;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace dg.utilities
{
    public class Log : ILog
    {
        private Logger log = LogManager.GetLogger("dg");

        private ConcurrentBag<TraceObj> pool = new ConcurrentBag<TraceObj>();

        public void Debug(string message)
        {
            log.Debug(message);
        }

        public void Error(string message)
        {
            log.Error(message);
        }

        public void Error(string message, Exception ex)
        {
            log.Error(message, ex);
        }

        public void Info(string message)
        {
            log.Info(message);
        }

        public void Trace(string message)
        {
            log.Trace(message);
        }

        public void Warn(string message)
        {
            log.Warn(message);
        }

        public IDisposable Watch(string message, string info = null)
        {
            TraceObj obj;
            if (pool.TryTake(out obj))
            {
                obj.Reuse(message, info);
                return obj;
            }
            else
            {
                return new TraceObj(this, message, info, pool.Add);
            }
        }

        public sealed class TraceObj : IDisposable
        {
            private Action<TraceObj> callback;
            private ILog log;
            private string message;
            private Stopwatch watch = new Stopwatch();

            public TraceObj(ILog log, string message, string info, Action<TraceObj> disposedCallback)
            {
                this.log = log;
                this.callback = disposedCallback;
                Reuse(message, info);
            }

            public void Dispose()
            {
                watch.Stop();
                log.Info(string.Format("End {0} - {1} ms", message, watch.ElapsedMilliseconds));
                if (callback != null)
                {
                    callback(this);
                }
            }

            public void Reuse(string message, string info)
            {
                this.message = message;
                log.Info(string.Format("Start {0}", message));
                if (!string.IsNullOrEmpty(info))
                {
                    log.Trace(string.Format(info));
                }
                watch.Restart();
            }
        }
    }
}