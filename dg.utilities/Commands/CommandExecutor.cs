﻿using dg.interfaces.Commands;
using dg.objects.Commands;
using System;
using System.Collections.Generic;

namespace dg.utilities.Commands
{
    public class CommandExecutor : ICommandExecutor
    {
        private readonly List<CommandConfiguration> executedCommands;

        private readonly List<CommandConfiguration> undoneCommands;

        public CommandExecutor()
        {
            this.executedCommands = new List<CommandConfiguration>();
            this.undoneCommands = new List<CommandConfiguration>();
        }

        public event EventHandler CanUndoRedoChanged = delegate { };

        public bool CanRedo
        {
            get
            {
                return this.undoneCommands.Count > 0;
            }
        }

        public bool CanUndo
        {
            get
            {
                return this.executedCommands.Count > 0;
            }
        }

        public void Abort()
        {
        }

        public void Execute<TCommand>(Action<ICommandConfiguration<TCommand>> action)
            where TCommand : Command
        {
            var config = new CommandConfiguration<TCommand>();

            action(config);

            config.Execute();

            this.executedCommands.Add(config);

            this.undoneCommands.Clear();

            this.CanUndoRedoChanged(this, EventArgs.Empty);
        }

        public void Redo()
        {
            if (this.undoneCommands.Count == 0)
            {
                return;
            }

            int index = this.undoneCommands.Count - 1;
            var cmd = this.undoneCommands[index];
            this.undoneCommands.RemoveAt(index);
            cmd.Execute();
            this.executedCommands.Add(cmd);
        }

        public void Undo()
        {
            if (this.executedCommands.Count == 0)
            {
                return;
            }

            int index = this.executedCommands.Count - 1;
            var cmd = this.executedCommands[index];
            this.executedCommands.RemoveAt(index);
            this.undoneCommands.Add(cmd);
            cmd.Unexecute();

            this.CanUndoRedoChanged(this, EventArgs.Empty);
        }
    }
}