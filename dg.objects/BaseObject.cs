﻿using System.Collections.Generic;

namespace dg.objects
{
    public class BaseObject : KeyObject
    {
        private IDictionary<string, string> settings = new Dictionary<string, string>();

        public IDictionary<string, string> Settings
        {
            get { return settings; }
            set { settings = value; }
        }
    }
}