﻿using System.Collections.Generic;

namespace dg.objects.Data
{
    public class EntityData : BaseObject
    {
        private IEnumerable<PropertyData> propertyData;

        public string EntityName { get; set; }

        public IEnumerable<PropertyData> PropertyData
        {
            get { return propertyData; }
            set { propertyData = value; }
        }
    }
}