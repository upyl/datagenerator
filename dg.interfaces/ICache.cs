﻿namespace dg.interfaces
{
    public interface ICache
    {
        T Pick<T>();

        T Pick<T>(string key);

        void Push<T>(T value);

        void Push<T>(string key, T value);
    }
}