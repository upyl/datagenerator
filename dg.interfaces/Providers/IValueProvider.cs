﻿namespace dg.interfaces.Providers
{
    public interface IValueProvider
    {
        string GetNext();
    }
}