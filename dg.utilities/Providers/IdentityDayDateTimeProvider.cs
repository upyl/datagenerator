﻿using dg.interfaces;
using dg.interfaces.Providers;
using dg.objects;
using System;
using System.Collections.Generic;

namespace dg.utilities.Providers
{
    public class IdentityDayDateTimeProvider : IValueProvider
    {
        private DateTime end;
        private Random generator = new Random();
        private int increment;

        private IDictionary<string, string> settings;

        private DateTime start;

        public IdentityDayDateTimeProvider(ICache cache, IDictionary<string, string> settings)
        {
            this.settings = settings;
            start = Convert.ToDateTime(settings[Keys.DateTimeMinValue]);
            end = Convert.ToDateTime(settings[Keys.DateTimeMaxValue]);
            increment = settings.ContainsKey(Keys.Increment) ? Convert.ToInt32(settings[Keys.Increment]) : 1;
        }

        public string GetNext()
        {
            int range = (DateTime.Today - start).Days;
            return start.AddDays(increment).ToString(settings[Keys.DateTimeFormat]);
        }
    }
}