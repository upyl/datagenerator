﻿using System;

namespace dg.interfaces
{
    public interface ILog
    {
        void Debug(string message);

        void Error(string message);

        void Error(string message, Exception ex);

        void Info(string message);

        void Trace(string message);

        void Warn(string message);

        IDisposable Watch(string message, string info = null);
    }
}