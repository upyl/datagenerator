﻿using dg.interfaces.Visitors;
using dg.objects;
using dg.objects.DatabaseDescription;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dg.utilities.Validators
{
    public class SchemaDescriptionValidator : IValidator<DatabaseDescription>
    {
        public void Validate(DatabaseDescription value)
        {
            CheckSettings(value);
            CheckProxy(value);
        }

        private static void CheckSettings(DatabaseDescription value)
        {
            var keys = Keys.GetAllKeys();
            CheckThatSettingIsSupported(value.Settings, keys);
            foreach (var entity in value.EntityDescriptions)
            {
                CheckThatSettingIsSupported(entity.Settings, keys);
                foreach (var property in entity.PropertyDescriptions)
                {
                    CheckThatSettingIsSupported(property.Settings, keys);
                }
            }
        }

        private static void CheckThatSettingIsSupported(IDictionary<string, string> settings, System.Collections.Generic.IDictionary<string, string> keys)
        {
            foreach (var setting in settings)
            {
                if (keys.Values.All(x => x != setting.Key))
                    throw new NotSupportedException(string.Format("setting {0}", setting.Key));
                Keys.CheckKeySupportValue(setting.Key, setting.Value);
            }
        }

        private void CheckProxy(DatabaseDescription value)
        {
            foreach (var entity in value.EntityDescriptions)
            {
                foreach (var property in entity.PropertyDescriptions)
                {
                    if (property.Settings.Any(x => x.Key == Keys.Entity))
                    {
                        if (value.EntityDescriptions.All(x => x.EntityName != property.Settings[Keys.Entity]))
                            throw new NotSupportedException(string.Format("Entity for Proxy {0}", property.Settings[Keys.Entity]));
                    }

                    if (property.Settings.Any(x => x.Key == Keys.Property))
                    {
                        if (value.EntityDescriptions.SelectMany(x => x.PropertyDescriptions).All(x => x.PropertyName != property.Settings[Keys.Property]))
                            throw new NotSupportedException(string.Format("Property for Proxy {0}", property.Settings[Keys.Property]));
                    }
                }
            }
        }
    }
}