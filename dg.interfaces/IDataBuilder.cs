﻿namespace dg.interfaces
{
    public interface IDataBuilder
    {
        void CreateStructure();

        string GetStatus();

        void ValidateStructure();

        void WriteStructure();
    }
}