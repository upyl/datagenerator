﻿using System.Collections.Generic;

namespace dg.objects.Data
{
    public class DatabaseData : BaseObject
    {
        private IEnumerable<EntityData> entityData;

        public string DatabaseName { get; set; }

        public IEnumerable<EntityData> EntityData
        {
            get { return entityData; }
            set { entityData = value; }
        }
    }
}