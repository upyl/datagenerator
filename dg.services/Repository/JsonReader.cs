﻿using dg.interfaces;
using Newtonsoft.Json;
using System.IO;

namespace dg.services.Repository
{
    public class JsonReader<T> : IReadableRepository<T>
    {
        private string fileName;

        public JsonReader(string fileName)
        {
            this.fileName = fileName;
        }

        public virtual T Read()
        {
            using (var f = File.OpenText(fileName))
            {
                return JsonConvert.DeserializeObject<T>(f.ReadToEnd());
            }
        }
    }
}