﻿namespace dg.objects.DatabaseDescription
{
    public class PropertyDescription : BaseObject
    {
        public EntityDescription EntityDescription { get; set; }

        public string PropertyName { get; set; }

        public string ValueProviderName { get; set; }
    }
}