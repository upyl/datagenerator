﻿using dg.interfaces.Commands;
using dg.mssql.Extensions;
using dg.objects;
using dg.objects.Commands;
using dg.objects.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dg.mssql.Commands
{
    public class CreateTableCommand : Command
    {
        private const string createTable =
            @"USE {0} IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{2}' and TABLE_CATALOG = '{0}'
and TABLE_SCHEMA = '{1}') CREATE TABLE {0}.{1}.{2} ({3})";

        private const string deleteTable = @"USE {0} IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{2}' and TABLE_CATALOG = '{0}'
and TABLE_SCHEMA = '{1}') DROP TABLE {0}.{1}.{2}";

        private const string propertyTemplate = @"{0} {1} {2}";
        private IList<string[]> columns;
        private ICommandPerformer<string> commmandExecutor;
        private string database;
        private string schema;
        private string table;

        public CreateTableCommand(ICommandPerformer<string> exec, Entity value)
        {
            this.database = value.Schema.Name;
            this.schema = value.Settings[Keys.NameSpace];
            this.table = value.Name;
            this.columns = value.Properties.Select(x =>
            {
                var result = new List<string> { x.Name, x.Type };
                if (x.Settings.ContainsKey(Keys.Identity))
                {
                    result.Add(Keys.Identity + x.Settings[Keys.Identity]);
                }
                if (x.Settings.ContainsKey(Keys.IsNotSupportNull) && x.Settings[Keys.IsNotSupportNull].ToBoolean())
                {
                    result.Add("NOT NULL");
                }
                else
                {
                    result.Add("NULL");
                }
                return result.ToArray();
            }).ToArray();

            this.commmandExecutor = exec;
        }

        public override string ToString()
        {
            return string.Format("Database - {0}, Schema - {1}, Table - {2}, Columns - {3} ", database, schema, table, string.Join(";", columns.Select(x => string.Join("|", x))));
        }

        protected override void ExecuteCore()
        {
            var properties = string.Empty;
            foreach (var column in columns)
            {
                if (column.Length < 1)
                {
                    throw new ArgumentOutOfRangeException("column.length");
                }
                properties += ", " +
                    string.Format(propertyTemplate, column[0], column[1], column.Length > 2 ? string.Join(" ", column.Skip(2)) : string.Empty);
            }
            if (properties.StartsWith(","))
            {
                properties = properties.Substring(1);
            }

            commmandExecutor.Execute(string.Format(createTable, database, schema, table, properties));
        }

        protected override void UnexecuteCore()
        {
            commmandExecutor.Execute(string.Format(deleteTable, database, schema, table));
        }
    }
}