﻿using dg.interfaces;
using dg.interfaces.Providers;
using dg.objects;
using System.Collections.Generic;

namespace dg.utilities.Providers
{
    public class CustomStringProvider : IValueProvider
    {
        private int index = 0;
        private IDictionary<string, string> settings;
        private string[] values;

        public CustomStringProvider(ICache cache, IDictionary<string, string> settings)
        {
            this.settings = settings;
            this.values = settings.ContainsKey(Keys.Values) ? this.settings[Keys.Values].Split(Constants.ValuesSettingsSplitter) : new string[0];
        }

        public string GetNext()
        {
            if (index < values.Length)
            {
                return values[index++];
            }
            return string.Empty;
        }
    }
}