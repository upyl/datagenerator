﻿namespace dg.objects.Database
{
    public class Relation : BaseObject
    {
        private string primaryEntity;
        private string primaryProperty;
        private string secondaryEntity;
        private string secondaryProperty;

        public Entity Entity { get; set; }

        public string ForeignEntity
        {
            get { return secondaryEntity; }
            set { secondaryEntity = value; }
        }

        public string ForeignProperty
        {
            get { return secondaryProperty; }
            set { secondaryProperty = value; }
        }

        public string PrimaryEntity
        {
            get { return primaryEntity; }
            set { primaryEntity = value; }
        }

        public string PrimaryProperty
        {
            get { return primaryProperty; }
            set { primaryProperty = value; }
        }
    }
}