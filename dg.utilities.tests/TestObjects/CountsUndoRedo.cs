﻿using dg.objects.Commands;

namespace dg.utilities.tests.TestObjects
{
    public class CountsUndoRedo : Command
    {
        public int ExecuteCount { get; set; }

        public int UndoCount { get; set; }

        protected override void ExecuteCore()
        {
            this.ExecuteCount++;
        }

        protected override void UnexecuteCore()
        {
            this.UndoCount++;
        }
    }
}