﻿using dg.interfaces.Commands;
using dg.interfaces.Providers;
using dg.mssql.Commands;
using dg.utilities.Commands;
using dg.utilities.Providers;
using Microsoft.Practices.Unity;

namespace dg.dependencies
{
    public class UtilsInstaller : IInstaller
    {
        public Microsoft.Practices.Unity.IUnityContainer Install(Microsoft.Practices.Unity.IUnityContainer container)
        {
            return container
                .RegisterType<ICommandExecutor, CommandExecutor>()
                .RegisterType(typeof(ICommandConfiguration<>), typeof(CommandConfiguration<>))
                .RegisterType<IValueProviderFabric, ValueProviderFabric>();
        }
    }
}