﻿using System.Collections.Generic;

namespace dg.interfaces
{
    public interface IDataReadable
    {
        int FieldCount { get; }

        IEnumerator<object[]> GetDataEnumerator();
    }
}